import React from "react";
import { useHistory } from "react-router-dom";

export default function Navbar() {
  let history = useHistory();
  return (
    <div>
      <button
      onClick={()=>history.push({
        pathname:"/",
      })}
      >Home</button>
      <button
      onClick={()=>history.push("/About")}
      variant="contained">About</button>
      <button
      onClick={()=>history.push("/Hobby")}
      variant="contained">Hobby</button>
    </div>
  );
}
//<button onClick={() => history.push("/cart")}>history function</button>