import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import About from "./pages/About";
import Hobby from "./pages/Hobby";
import Home from "./pages/Home";

export default function Rout() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        <Route exact path="/Hobby">
          <Hobby />
        </Route>
        <Route exact path="/About">
          <About />
        </Route>
      </Switch>
    </Router>
  );
}
