import React, { useState } from "react";
import Button from "@mui/material/Button";
import { Modal } from "@mui/material";
import { Box } from "@mui/system";
import { TextField } from "@mui/material";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'white',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function BtnEdit(props){
    const [flag, setFlag] = useState(false)
    const [users, setUsers] = useState({
        name: "",
        address: "",
        hobby:""
    })
    const [inputState,setInputState]= useState({submit: false, et1:true, et2:true, et3:true})

    const newIds = props.dt.slice()
    const changedata = (input, oldName, name, address,hobby) => {
        return input.map(item => {
            var temp = Object.assign({}, item);
            if (temp.name === oldName) {
                temp.name = name;
                temp.address = address;
                temp.hobby = hobby;
            }
            return temp;
        });
    }

    return(
        <div>
            <Button
            variant= "contained"
            color="error"
            onClick={()=>
                {
                    setInputState({...inputState, submit: false})
                    setUsers(props.account)
                    setFlag(true)
                }
            }>
                Edit
            </Button>

            <Modal
                open={flag}
                onClose={()=>{
                    setUsers({...users,name:"", address:"",hobby:"" })
                    setFlag(false)
                    }}>
                    <Box sx={style}>
                        <Box sx ={{width: "100%"}}>

                            <TextField
                                sx={{mt: 2}}
                                error={inputState.submit && !users.name}
                                helperText="Tidak Boleh Kosong"
                                fullWidth label = 'users' 
                                variant='outlined'
                                defaultValue={props.account.name}
                                onChange={(e)=> {
                                    console.log(e.target.value) 
                                    setUsers({...users,name: e.target.value,})
                                    if(e.target.value){
                                        setInputState({...inputState, et1: true})
                                    } else{
                                        setInputState({...inputState, et1: false})
                                    };
                                }}/>

                            <TextField
                                sx={{mt: 2}}
                                error={inputState.submit && !users.address}
                                helperText="Tidak Boleh Kosong"
                                fullWidth label = 'address' 
                                variant='outlined'
                                defaultValue = {props.account.address}
                                onChange={(e)=> {setUsers({
                                    ...users,
                                    address: e.target.value,
                                })
                                if(e.target.value){
                                    setInputState({...inputState, et2: true})
                                } else{
                                    setInputState({...inputState, et2: false})
                                };
                                }}/>

                            <TextField
                                sx={{mt: 2}}
                                error={inputState.submit && !users.hobby}
                                helperText="Tidak Boleh Kosong"
                                fullWidth label = 'Hobby' 
                                variant='outlined'
                                defaultValue={props.account.hobby}
                                onChange={(e)=> {
                                    setUsers({...users, hobby: e.target.value,});
                                    if(e.target.value){
                                        setInputState({...inputState, et3: true})
                                    } else{
                                        setInputState({...inputState, et3: false})
                                    };
                                }}/>

                        </Box>
                        <Button 
                            variant ='contained'
                            color="success"
                            sx={{mt: 5, mr: 1}}
                            onClick={()=> {
                                if(inputState.et1 && inputState.et2 &&inputState.et3){
                                    var upval = changedata(newIds, props.account.name, users.name, users.address,users.hobby);
                                    props.update(upval)
                                    setInputState({...inputState, submit: false})
                                    setUsers({...users,name:"", address:"",hobby:"" })
                                    setFlag(false)}
                                else {
                                    setInputState({...inputState, submit: true})
                                }
                                
                            }}
                            >Submit
                        </Button>
                        <Button
                        variant="contained"
                        color = 'error'
                        sx={{mt: 5}}

                        onClick={()=> {
                            setFlag(false)
                            setInputState({...inputState, submit: false})
                            setUsers({...users,name:"", address:"",hobby:"" })
                            }}>
                            Close
                        </Button>
                    </Box>
             </Modal>

        </div> 
    )
}
