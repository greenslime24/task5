import React from "react";
import Button from "@mui/material/Button";
import { NavLink } from "react-router-dom";

export default function BtnView(props) {
  return (
    <div>
      <Button 
      component={NavLink}
       variant="contained"
       to={{
        pathname:"/view",
        state:{
            nama: props.nama,
            address: props.address,
            hobby: props.hobby,
        },
       }}>
        View
      </Button>
    </div>
  );
}
