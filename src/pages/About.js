import React from "react";
import Navbar from "../assets/Navbar";

export default function About() {
  return (
    <div className="cnt">
      <div className="secondNavbar">
        <Navbar />
      </div>
      <div>
        <div>This app is used to manage users name, address, and hobby</div>
      </div>
    </div>
  );
}
