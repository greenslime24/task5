import React, {useState} from 'react';
import './App.css';
import BtnFunction from "../component/button"
import Search from "../component/search"
import BtnEdit from "../component/editbtn"
import Navbar from '../assets/Navbar';
import Rout from '../Router';


function Home() {
  const [src, setSrc]= useState("")
  const [form, setForm] = useState([])

  const gtbdat=(name, address, hobby)=>{
    setForm([...form,{name, address, hobby}])
  }

  const getarry=(a)=>{
    setForm(a)
  }

  const array = []

  const inputvalue = (a)=>{
    array.push(a)
  }

  const getSearch=(inp)=>{
    setSrc(inp)
  }

  const result = form.filter((x)=> x.name.toLowerCase().includes(src.toLowerCase()));

  return (
    <div className="cnt">
      <div className='Navbar'>
        <div className='MyApp'>My App 
        <Navbar/>
        </div>
        <div className='btn-lyout'><BtnFunction grbdt={gtbdat} inpvalue={inputvalue}/></div>
      </div>
      <div className='search'>
        <Search getsrc={getSearch}/>
      </div>
        {src
          ? result.map((x)=>{
            return(
              <div className='content'>
              <dev className="left">
                <p className='names'>{x.name}</p>
                <p className='address'>{x.address}</p>
              </dev>
              <dev className="right">
                <p className="hobby">{x.hobby}</p>
              </dev>
            </div>
            )
          })
          : form.map((x)=>{
            return(
            <div className='content'>
            <dev className="left">
              <p className='names'>{x.name}</p>
              <p className='address'>{x.address}</p>
            </dev>
            <dev className="right">
              <p className="hobby">{x.hobby}</p>
              <BtnEdit account={x} dt={form} update={getarry}/>
            </dev>
          </div>)
          })
        }
    </div>
  );
}

export default Home;
