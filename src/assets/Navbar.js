import React from "react";
import { useHistory } from "react-router-dom";

import "../App.css";


export default function Navbar() {
   let history = useHistory();
  return (
    <div>
      <div>
        <button onClick={() => history.push("/")}>Home</button>
        <button onClick={() => history.push("/About")} variant="contained">
          About
        </button>
        <button onClick={() => history.push("/Hobby")} variant="contained">
          Hobby
        </button>
      </div>
    </div>
  );
}

